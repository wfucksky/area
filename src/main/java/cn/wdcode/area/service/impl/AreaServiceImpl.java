package cn.wdcode.area.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import cn.wdcode.area.mapper.AreaMapper;
import cn.wdcode.area.entity.Area;
import cn.wdcode.area.service.IAreaService;
import com.baomidou.framework.service.impl.SuperServiceImpl;

import java.io.Serializable;

/**
 *
 * Area 表数据服务层接口实现类
 *
 */
@Service
public class AreaServiceImpl extends SuperServiceImpl<AreaMapper, Area> implements IAreaService {




}