package cn.wdcode.area.entity;

import java.io.Serializable;


import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 *
 * 地区编码
 *
 */
@TableName(value = "area")
public class Area implements Serializable {

	@TableField(exist = false)
	private static final long serialVersionUID = 1L;

	/** 编号 */
	@TableId(value = "area_id")
	private String areaId;

	/** 省和直辖市 */
	@TableField(value = "area_province")
	private String areaProvince;

	/** 市或者县 */
	@TableField(value = "area_citytype")
	private String areaCitytype;

	/** 区 */
	@TableField(value = "area_city")
	private String areaCity;

	/**  */
	@TableField(value = "area_parent_id")
	private String areaParentId;


	public String getAreaId() {
		return this.areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	public String getAreaProvince() {
		return this.areaProvince;
	}

	public void setAreaProvince(String areaProvince) {
		this.areaProvince = areaProvince;
	}

	public String getAreaCitytype() {
		return this.areaCitytype;
	}

	public void setAreaCitytype(String areaCitytype) {
		this.areaCitytype = areaCitytype;
	}

	public String getAreaCity() {
		return this.areaCity;
	}

	public void setAreaCity(String areaCity) {
		this.areaCity = areaCity;
	}

	public String getAreaParentId() {
		return this.areaParentId;
	}

	public void setAreaParentId(String areaParentId) {
		this.areaParentId = areaParentId;
	}

}
